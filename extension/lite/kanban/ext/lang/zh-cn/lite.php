<?php
$lang->kanban->taskColumn = array();
$lang->kanban->taskColumn['wait']       = '未开始';
$lang->kanban->taskColumn['developing'] = '进行中';
$lang->kanban->taskColumn['developed']  = '已完成';
$lang->kanban->taskColumn['pause']      = '已暂停';
$lang->kanban->taskColumn['canceled']   = '已取消';
$lang->kanban->taskColumn['closed']     = '已关闭';
